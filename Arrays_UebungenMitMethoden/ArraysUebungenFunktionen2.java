public class ArraysUebungenFunktionen2 {

    public static void main(String[] args) {

        int[] nums = {1,2,3,4,5};
        reverse(nums);
        System.out.println(convertArrayToString(nums));
    }

    public static void reverse(int[] array){

        int temp;
        for (int i = 0; i < (array.length)/2; i++){
            temp = array[i];
            array[i] = array[array.length - 1 - i];
            array[array.length - 1 - i] = temp;
        }
    }

    public static String convertArrayToString(int[] zahlen){

        String output = "";
        for (int i = 0; i < zahlen.length - 1; i++) {
            output += zahlen[i] + ", ";
        }
        output += zahlen[zahlen.length - 1];

        return output;
    }
}
