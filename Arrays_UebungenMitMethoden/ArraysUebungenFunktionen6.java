import java.util.Arrays;
import java.util.Scanner;

public class ArraysUebungenFunktionen6 {

    public static void main(String[] args) {

        int[][] matrix = matrix(2, 2);
        System.out.println(Arrays.deepToString(matrix));

        if (transponiert(matrix)) System.out.println("TRANSPONIERT");
    }

    public static boolean transponiert(int[][] matrix){

        int[][] copyMatrix = new int[matrix[0].length][matrix.length];

        for (int i = 0; i < matrix.length; i++){
            for (int k = 0; k < matrix[0].length; k++){

                copyMatrix[i][k] = matrix[k][i];
            }
        }
        System.out.println(Arrays.deepToString(copyMatrix));
        return Arrays.deepToString(matrix).equals(Arrays.deepToString(copyMatrix));

    }

    public static int[][] matrix(int n, int m){

        Scanner scanner = new Scanner(System.in);
        int[][] output = new int[n][m];

        for (int i = 0; i < n; i++){
            for (int k = 0; k < m; k++){

                output[i][k] = scanner.nextInt();
            }
        }

        scanner.close();
        return output;
    }
}
