import java.util.Arrays;
import java.util.Scanner;

public class ArraysUebungenFunktionen5 {

    public static void main(String[] args) {

        System.out.println(Arrays.deepToString(matrix(2,2)));
    }

    public static int[][] matrix(int n, int m){

        Scanner scanner = new Scanner(System.in);
        int[][] output = new int[n][m];

        for (int i = 0; i < n; i++){
            for (int k = 0; k < m; k++){

                output[i][k] = scanner.nextInt();
            }
        }

        scanner.close();
        return output;
    }
}
