import java.util.Arrays;

public class ArraysUebungenFunktionen4 {

    public static void main(String[] args) {

        System.out.println(Arrays.deepToString(temperatures(3)));
    }

    public static double[][] temperatures(int amount){

        double[][] output = new double[amount][2];

        for (int i = 0; i < amount; i++){
            output[i][0] = i * 10.0;
            output[i][1] = (5.0/9.0) * (i * 10.0 - 32.0);
        }

        return output;
    }
}
