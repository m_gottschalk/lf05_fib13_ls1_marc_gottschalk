public class ArraysUebungenFunktionen1 {
    public static void main(String[] args) {

        int[] nums = {1,2,3,4,5};
        System.out.println(convertArrayToString(nums));
    }

    public static String convertArrayToString(int[] zahlen){

        String output = "";
        for (int i = 0; i < zahlen.length - 1; i++) {
            output += zahlen[i] + ", ";
        }
        output += zahlen[zahlen.length - 1];

        return output;
    }
}
