public class ArraysUebungenFunktionen3 {
    public static void main(String[] args) {

        int[] nums = {1,2,3,4,5};

        System.out.println(convertArrayToString(reverse(nums)));
    }

    public static int[] reverse(int[] array){

        int[] newArray = new int[array.length];

        for(int i = array.length - 1; i >= 0; i--){
            newArray[i] = array[array.length - 1 - i];
        }

        return newArray;
    }

    public static String convertArrayToString(int[] zahlen){

        String output = "";
        for (int i = 0; i < zahlen.length - 1; i++) {
            output += zahlen[i] + ", ";
        }
        output += zahlen[zahlen.length - 1];

        return output;
    }
}
