import java.util.Scanner;

public class BMI {

    public static void main(String[] args) {

        //Folgendes ist natürlich absolut over the top, ich wollte aber die Möglichkeit
        //nutzen und mir die Vererbung in Java im Gegensatz zu Kotlin anzuschauen

        Scanner scanner = new Scanner(System.in);

        System.out.println("\n\nm oder w: ");
        char sex = scanner.next().charAt(0);
        System.out.println("\nGewicht in kg: ");
        double weight = scanner.nextDouble();
        System.out.println("\nGroesse in m: ");
        double height = scanner.nextDouble();

        if (sex == 'm'){
            Man x = new Man(weight, height);
            System.out.println(x.bmiClassification());
        }
        else if(sex == 'w'){
            Woman x = new Woman(weight, height);
            System.out.println(x.bmiClassification());
        }
        else{
            System.out.println("\n\nGeschlecht not found");
            return;
        }

        scanner.close();
    }

    abstract static class Person {
        protected double weight;
        protected double height;

        Person(double weight, double height){
            this.weight = weight;
            this.height = height;
        }

        protected double bmi(){

            return this.weight / (this.height * this.height);
        }
        abstract String bmiClassification();
    }

    private static class Woman extends Person {

        Woman(double weight, double height) {
            super(weight, height);
        }

        @Override
        String bmiClassification() {

            double bmi = this.bmi();
            String out = String.valueOf(bmi);

            if (bmi < 19.0){
                out += " - Untergewicht";
            }
            else if(bmi > 24.0){
                out += " - Übergewicht";
            }
            else{
                out += " - Normalgewicht";
            }

            return out;
        }
    }

    private static class Man extends Person {

        Man(double weight, double height) {
            super(weight, height);
        }

        @Override
        String bmiClassification() {

            double bmi = this.bmi();
            String out = String.valueOf(bmi);

            if (bmi < 20.0){
                out += " - Untergewicht";
            }
            else if(bmi > 25.0){
                out += " - Übergewicht";
            }
            else{
                out += " - Normalgewicht";
            }

            return out;
        }
    }
}
