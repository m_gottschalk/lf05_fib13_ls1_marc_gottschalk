import java.util.Scanner;

public class OhmschesGesetz {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        Quantity eResistance = new Quantity(0.0, "R", "Ohm");
        Quantity eCurrent = new Quantity(0.0, "I", "Ampere");
        Quantity eVoltage = new Quantity(0.0, "U", "Volt");

        Quantity[] ohmStuff = {eResistance, eCurrent, eVoltage};


        System.out.println("\n\nErrechnung von..? I/R/U ");
        String lookingFor = scanner.next();

        //Prüfen, welche Einheiten noch nicht gesetzt sind
        for (Quantity i:ohmStuff) {
            if(i.value == 0.0 && i.letter.charAt(0) != lookingFor.charAt(0)){
                System.out.println(i.letter + ": ");
                i.value = scanner.nextDouble();
            }
        }

        //Anhand der gesuchten Einheit, Formel verwenden
        switch (lookingFor) {
            case "I" -> System.out.println("I = U/R = " + (eVoltage.value / eResistance.value) + " " + eCurrent.unit);
            case "U" -> System.out.println("U = R*I = " + (eResistance.value * eCurrent.value) + " " + eVoltage.unit);
            case "R" -> System.out.println("R = U/I = " + (eVoltage.value / eCurrent.value) + " " + eResistance.unit);
            default -> System.out.println("Falsche Eingabe");
        }

        scanner.close();
    }

    //neue Klasse zur Speicherung von Wert und Formelzeichen in einem Element
    public static class Quantity{
        double value;
        String letter;
        String unit;

        Quantity(double value, String letter, String unit){
            this.value = value;
            this.letter = letter;
            this.unit = unit;
        }
    }
}
