import java.util.Scanner;

public class Einmaleins {

    public static void main(String[] args) {

        String row = "";
        for (int i = 1; i < 11; i++){
            for (int x = 1; x < 11; x++){
                row += x * i + " ";
                if (x == 10){
                    row += "\n";
                }
            }
        }
        System.out.println(row);

    }
}
