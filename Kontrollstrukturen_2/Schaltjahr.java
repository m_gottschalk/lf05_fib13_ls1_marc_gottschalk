import java.util.Scanner;

public class Schaltjahr {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("\n\nJahr: ");
        int year = scanner.nextInt();

        if (
                ( year % 4 == 0 ) &&
                        (( year % 100 != 0 ) ||
                        ( year % 400 == 0))
        ){
            System.out.println("\n\nIst ein Schaltjahr.");
        }else{
            System.out.println("\n\nIst kein Schaltjahr.");
        }
    }
}
