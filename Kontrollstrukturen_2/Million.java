import java.beans.PropertyEditorSupport;
import java.util.Scanner;

public class Million {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        char input;

        do {
            System.out.println("\n\nEinlage: ");
            double money = scanner.nextDouble();
            System.out.println("\nZinssatz in %: ");
            double interest = scanner.nextDouble();

            System.out.println("\nBis zur Million brauchst du " + yearsToMil(money, interest) + " Jahre.");

            System.out.println("\nWeitere Eingabe? (j/n) ");
            input = scanner.next().charAt(0);
        }while(input != 'n');

        scanner.close();
    }

    public static int yearsToMil(double money, double interest){

        int result = 0;
        while(money < 1000000.0){
            money += money * interest * 0.01;
            result++;
        }
        return result;
    }
}
