import java.util.Scanner;

public class Quadrat {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("\n\nSeitenlaenge: ");
        byte num = scanner.nextByte();

        String row = "";

        for (int i = 0; i < num; i++){
            for (int x = 0; x < num; x++){

                if (i == 0 || i == num - 1){
                    row += "*  ";
                }
                else{
                    if (x == 0 || x == num - 1){
                        row += "*  ";
                    }
                    else{
                        row += "   ";
                    }
                }
            }
            row += "\n";
        }
        System.out.println(row);
    }
}
