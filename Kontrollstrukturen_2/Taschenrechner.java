import java.util.Scanner;

public class Taschenrechner {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("\n\nZahl 1: ");
        double num1 = scanner.nextDouble();
        System.out.println("\n\n+, -, *, /? ");
        char operator = scanner.next().charAt(0);
        System.out.println("\nZahl 2: ");
        double num2 = scanner.nextDouble();

        switch (operator){
            case '+': System.out.println(num1 + num2);
            break;
            case '-': System.out.println(num1 - num2);
            break;
            case '*': System.out.println(num1 * num2);
            break;
            case '/': System.out.println(num1 / num2);
            break;
            default: System.out.println("ungueltiger Operator!");
        }
        scanner.close();
    }
}
