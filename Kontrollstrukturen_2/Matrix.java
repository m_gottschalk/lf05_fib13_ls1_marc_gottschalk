import java.util.Scanner;

public class Matrix {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("\n\nZahl zw 2 und 9: ");
        byte wanted = scanner.nextByte();

        String str = "";

        for (int i = 0; i < 10; i++){

            for (int x = 0; x < 10; x++){
                str += iTS((byte) (x + 10 * i), wanted) + " ";
            }
            str += "\n";
        }

        System.out.println(str);

        scanner.close();
    }

    public static String iTS(byte num, byte wanted){

        if (isIn(num, wanted) || (num % wanted == 0) || crossSum(num) == wanted){
            return " *";
        }
        else if (num < 10 && num >= 0){
            return ("0" + num);
        }
        else{
            return ("" + num);
        }
    }

    public static boolean isIn(byte num, byte check){

        String numStr = Byte.toString(num);
        String checkStr = Byte.toString(check);

        return numStr.contains(checkStr);
    }

    public static byte crossSum(byte num){

        byte result = 0;

        for (byte i:byteAsArray(num)) {
            result += i;
        }

        return result;
    }

    public static byte[] byteAsArray(int number) {
        byte[] results = new byte[String.valueOf(number).length()];
        /* Start at the end of the array. i.e. 1234, should be converted to {1, 2, 3, 4} */
        for (int i = String.valueOf(number).length() - 1; i >= 0; i--) {
            results[i] = (byte) (number % 10);
            number = number / 10;
        }
        return results;
    }
}
