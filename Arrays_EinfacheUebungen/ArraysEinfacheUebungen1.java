public class ArraysEinfacheUebungen1 {

    public static void main(String[] args) {

        Byte[] test = new Byte[10];

        for (int i = 0; i < 10; i++){
            test[i] = (byte) i;
        }

        for (byte k:test) {
            System.out.println(k);
        }
    }
}
