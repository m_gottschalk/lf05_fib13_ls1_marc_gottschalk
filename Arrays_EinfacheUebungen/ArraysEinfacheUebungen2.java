public class ArraysEinfacheUebungen2 {

    public static void main(String[] args) {

        Byte[] test = new Byte[10];

        for (int i = 1; i <= 19; i += 2){
            test[i/2] = (byte) i;
        }

        for (byte k:test) {
            System.out.println(k);
        }
    }
}
