public class ArraysEinfacheUebungen4 {
    public static void main(String[] args) {

        int[] lotto = {3, 7, 12, 18, 37, 42};

        if (isInArray(lotto, 12)){
            System.out.println("12 ist enthalten.");
        }
        else {
            System.out.println("12 ist nicht enthalten.");
        }

        if (isInArray(lotto, 13)){
            System.out.println("13 ist enthalten.");
        }
        else {
            System.out.println("13 ist nicht enthalten.");
        }
    }

    public static boolean isInArray(int[] array, int checkFor){

        for (int i = 0; i < array.length; i++){
            if (array[i] == checkFor) return true;
        }

        return false;
    }
}
