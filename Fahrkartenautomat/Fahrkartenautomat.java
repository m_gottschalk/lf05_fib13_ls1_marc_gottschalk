import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
        Scanner tastatur = new Scanner(System.in);
        double zuZahlenderBetrag;
        byte auswahl;
        String[] ticketbezeichnungen = {
                "(1) Einzelfahrschein AB",
                "(2) Einzelfahrschein BC",
                "(3) Einzelfahrschein ABC",
                "(4) Kurzstrecke",
                "(5) Tageskarte AB",
                "(6) Tageskarte BC",
                "(7) Tageskarte ABC",
                "(8) Kleingruppen-Tagesticket AB",
                "(9) Kleingruppen-Tagesticket BC",
                "(10) Kleingruppen-Tagesticket ABC"
        };


        do {
            System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin aus:\n\n" +
                    arrayAusgabe(ticketbezeichnungen) +

                    "\n\nIhre Wahl:\s");

            auswahl = tastatur.nextByte();

            switch (auswahl) {
                case 0 -> System.out.println("\n\nCiao!");
                default -> {
                    if (auswahl <= 10 && auswahl >= 1){
                        zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur, auswahl);
                        rueckgeldAusgeben(fahrkartenBezahlen(zuZahlenderBetrag, tastatur));
                        fahrkartenAusgeben();
                    }
                    else {
                        System.out.println("\n\nFalsche Eingabe!");
                    }
                }
            }
        }while(auswahl != 0);

        tastatur.close();
    }
    private static double fahrkartenbestellungErfassen(Scanner tastatur, int auswahl){

        double[] ticketpreise = {
                2.90,
                3.30,
                3.60,
                1.90,
                8.60,
                9.00,
                9.60,
                23.50,
                24.30,
                24.90
        };

        System.out.print("\nAnzahl der Tickets: ");
        return ticketpreise[auswahl - 1] * tastatur.nextByte();
    }
    private static double fahrkartenBezahlen(double preis, Scanner tastatur){

        double eingeworfeneMuenze;
        double rueckgabebetrag;
        // Geldeinwurf
        // -----------
        double eingezahlterGesamtbetrag = 0.0;
        while(eingezahlterGesamtbetrag < preis)
        {
            System.out.printf("Noch zu zahlen: %.2f Euro\n", (preis - eingezahlterGesamtbetrag));
            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            eingeworfeneMuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMuenze;
        }
        rueckgabebetrag = eingezahlterGesamtbetrag - preis;
        return rueckgabebetrag;
    }
    private static void rueckgeldAusgeben(double betrag){

        // Rückgeldberechnung und -Ausgabe
        // -------------------------------
        if(betrag > 0.0)
        {
            System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro", betrag);
            System.out.println(" wird in folgenden Münzen ausgezahlt:");

            while(betrag >= 2.0) // 2 EURO-Münzen
            {
                System.out.println("2 EURO");
                betrag -= 2.0;
            }
            while(betrag >= 1.0) // 1 EURO-Münzen
            {
                System.out.println("1 EURO");
                betrag -= 1.0;
            }
            while(betrag >= 0.5) // 50 CENT-Münzen
            {
                System.out.println("50 CENT");
                betrag -= 0.5;
            }
            while(betrag >= 0.2) // 20 CENT-Münzen
            {
                System.out.println("20 CENT");
                betrag -= 0.2;
            }
            while(betrag >= 0.1) // 10 CENT-Münzen
            {
                System.out.println("10 CENT");
                betrag -= 0.1;
            }
            while(betrag >= 0.05)// 5 CENT-Münzen
            {
                System.out.println("5 CENT");
                betrag -= 0.05;
            }
        }
    }
    private static void fahrkartenAusgeben(){

        // Fahrscheinausgabe
        // -----------------
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("\n");
        System.out.println("""
                        Vergessen Sie nicht, den Fahrschein
                        vor Fahrtantritt entwerten zu lassen!
                        
                        Wir wünschen Ihnen eine gute Fahrt.
                        
                        """);
    }

    public static String arrayAusgabe(String[] array){

        String output = "";

        for (String s:array) {
            output += s + "\n";
        }

        return output;
    }
}