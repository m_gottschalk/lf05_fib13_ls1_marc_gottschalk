
public class Temperaturtabelle {

	public static void main(String[] args) {
		String kopfzeile = "Fahrenheit  |   Celsius";
		String linie = "-----------------------\n";

		double ersterWertF = -20;
		double zweiterWertF = -10;
		double dritterWertF = 0;
		double vierterWertF = 20;
		double fuenfterWertF = 30;
		//test

		double ersterWertC = -28.8889;
		double zweiterWertC = -23.3333;
		double dritterWertC = -17.7778;
		double vierterWertC = -6.6667;
		double fuenfterWertC = -1.1111;

		System.out.printf("%s\n%s", kopfzeile, linie);
		System.out.printf("%+-12.0f|%+10.2f\n", ersterWertF, ersterWertC);
		System.out.printf("%+-12.0f|%+10.2f\n", zweiterWertF, zweiterWertC);
		System.out.printf("%+-12.0f|%+10.2f\n", dritterWertF, dritterWertC);
		System.out.printf("%+-12.0f|%+10.2f\n", vierterWertF, vierterWertC);
		System.out.printf("%+-12.0f|%+10.2f\n", fuenfterWertF, fuenfterWertC);

	}

}
