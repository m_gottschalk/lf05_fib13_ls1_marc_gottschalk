package Models;

public class Node<T> {
    public Node<T> next = null;
    public T value;

    public Node(T val){
        this.value = val;
    }
}
