package Models;

import Interfaces.ICustomStack;

public class MyStackWithNodes<T> implements ICustomStack<T> {

    private Node<T> start = null;

    public MyStackWithNodes() {
    }

    @Override
    public T pop() {
        Node<T> cursor = start;

        // no element
        if(cursor == null){
            return null;
        }

        // move to last node
        while(cursor.next != null){
            cursor = cursor.next;
        }

        return cursor.value;
    }

    @Override
    public void push(T obj) {
        Node<T> cursor = start;

        // no element
        if(cursor == null){
            start = new Node<>(obj);
            return;
        }

        // move to last node
        while(cursor.next != null){
            cursor = cursor.next;
        }

        cursor.next = new Node<>(obj);
    }

    public int getCurrentLength() {

        int length = 0;

        Node<T> cursor = start;
        while(cursor != null){
            length++;
            cursor = cursor.next;
        }

        return length;
    }
}
