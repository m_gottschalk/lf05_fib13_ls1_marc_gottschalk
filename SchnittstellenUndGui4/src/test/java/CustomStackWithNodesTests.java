import Models.MyStackWithNodes;
import org.jetbrains.annotations.Nullable;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class CustomStackWithNodesTests {

    MyStackWithNodes<Integer> sut;

    @BeforeEach
    void beforeEach(){
        sut = new MyStackWithNodes<>();
    }

    @Test
    void pop_on_empty_stack_should_return_null(){
        @Nullable Integer result = sut.pop();
        assertNull(result);
    }

    @Test
    void push_pop(){
        int number = 5;

        sut.push(number);

        assertEquals(number, sut.pop());
    }

    @Test
    void push_changes_length(){
        sut.push(1);
        sut.push(1);
        sut.push(1);
        assertEquals(3, sut.getCurrentLength());

        sut.push(1);

        assertEquals(4, sut.getCurrentLength());
    }
}
