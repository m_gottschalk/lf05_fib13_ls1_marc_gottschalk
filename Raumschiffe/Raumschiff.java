import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Marc Gottschalk
 * @version 1.0
 */
public class Raumschiff {

    private int photonentorpedoAnzahl;
    private int energieversorgungInProzent;
    private int schildeInProzent;
    private int huelleInProzent;
    private int lebenserhaltungssystemeInProzent;
    private int androidenAnzahl;
    private String schiffsname;
    private static ArrayList<String> broadcastKommunikator = new ArrayList<>();
    private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<>();

    public Raumschiff(int photonentorpedoAnzahl,int energieversorgungInProzent,
                      int zustandSchildeInProzent,int zustandHuelleInProzent,
                      int zustandLebenserhaltungssystemeInProzent,
                      int anzahlDroiden,String schiffsname){
        this.photonentorpedoAnzahl = photonentorpedoAnzahl;
        this.energieversorgungInProzent = energieversorgungInProzent;
        this.schildeInProzent = zustandSchildeInProzent;
        this.huelleInProzent = zustandHuelleInProzent;
        this.lebenserhaltungssystemeInProzent = zustandLebenserhaltungssystemeInProzent;
        this.androidenAnzahl = anzahlDroiden;
        this.schiffsname = schiffsname;
    }

    public int getPhotonentorpedoAnzahl() {
        return photonentorpedoAnzahl;
    }

    public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
        this.photonentorpedoAnzahl = photonentorpedoAnzahl;
    }

    public int getEnergieversorgungInProzent() {
        return energieversorgungInProzent;
    }

    /**
     * Setter für energieversorgungInProzent. Minimalwert 0.
     * @param energieversorgungInProzent
     */
    public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
        this.energieversorgungInProzent = java.lang.Math.max(energieversorgungInProzent, 0);
    }

    public int getSchildeInProzent() {
        return schildeInProzent;
    }

    /**
     * Setter für schildeInProzent. Minimalwert 0.
     * @param schildeInProzent
     */
    public void setSchildeInProzent(int schildeInProzent) {
        this.schildeInProzent = java.lang.Math.max(schildeInProzent, 0);
    }

    public int getHuelleInProzent() {
        return huelleInProzent;
    }

    /**
     * Setter für huelleInProzent. Minimalwert 0.
     * @param huelleInProzent
     */
    public void setHuelleInProzent(int huelleInProzent) {
        this.huelleInProzent = java.lang.Math.max(huelleInProzent, 0);
    }

    public int getLebenserhaltungssystemeInProzent() {
        return lebenserhaltungssystemeInProzent;
    }

    /**
     * Setter für lebenserhaltungssystemeInProzent. Minimalwert 0.
     * @param lebenserhaltungssystemeInProzent
     */
    public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
        this.lebenserhaltungssystemeInProzent = java.lang.Math.max(lebenserhaltungssystemeInProzent, 0);
    }

    public int getAndroidenAnzahl() {
        return androidenAnzahl;
    }

    public void setAndroidenAnzahl(int androidenAnzahl) {
        this.androidenAnzahl = androidenAnzahl;
    }

    public String getSchiffsname() {
        return schiffsname;
    }

    public void setSchiffsname(String schiffsname) {
        this.schiffsname = schiffsname;
    }

    /**
     * Hinzufügen eines Ladungsobjektes zum Ladungsverzeichnis des Raumschiffs
     * @param neueLadung Hinzuzufügendes Ladungsobjekt
     */
    public void addLadung(Ladung neueLadung) {
        ladungsverzeichnis.add(neueLadung);
    }

    /**
     * Methode zum Verschießen von Photonentorpedos. Bei ausreichend Munition wird ein Torpedo abgezogen, eine Nachricht
     * an alle verschickt und anschließend die treffer Methode ausgelöst.
     * @param r Das getroffene Raumschiff
     */
    public void photonentorpedoSchiessen(Raumschiff r){
        if(photonentorpedoAnzahl > 0){
            photonentorpedoAnzahl = photonentorpedoAnzahl - 1;
            nachrichtAnAlle("Photonentorpedo abgeschossen");
            treffer(r);
        }else {
            nachrichtAnAlle("-=*Click*=-");
        }
    }

    /**
     * Methode zum Verschießen von Phaserkanonen. Bei ausreichend Energie wird die Hälfte der maximalen Energie abgezogen,
     * eine Nachricht an alle verschickt und anschließend die treffer Methode ausgelöst.
     * @param r Das getroffene Raumschiff
     */
    public void phaserkanoneSchiessen(Raumschiff r){
        if (energieversorgungInProzent >= 50){
            nachrichtAnAlle("Phaserkanone abgeschossen");
            energieversorgungInProzent = energieversorgungInProzent - 50;
            treffer(r);
        }else {
            nachrichtAnAlle("-=*Click*=-");
        }

    }

    /**
     * Pruefung der Verteidigungswerte des getroffenen Raumschiffs. Abhängig davon wird der Schaden an das jeweilige
     * Schiffsmodul angebracht.
     * @param r Das getroffene Raumschiff; notwendig, um auf seine Methoden zugreifen zu können
     */
    private void treffer(Raumschiff r){
        System.out.println(r.getSchiffsname() + " wurde getroffen!");
        r.setSchildeInProzent(r.getSchildeInProzent() - 50);

        if (r.getSchildeInProzent() <= 0){
            r.setHuelleInProzent(r.getHuelleInProzent() - 50);
            r.setEnergieversorgungInProzent(r.getEnergieversorgungInProzent() - 50);

            if (r.getHuelleInProzent() <= 0){
                r.setLebenserhaltungssystemeInProzent(0);
                nachrichtAnAlle("Lebenserhaltungssysteme von " + r.getSchiffsname() + " vollstaendig zerstoert.");
            }
        }
    }

    /**
     * Speichert eine Nachricht im broadcastKommunikator ab.
     * @param message Die zu speichernde Nachricht
     */
    public void nachrichtAnAlle(String message){
        broadcastKommunikator.add(message);
    }

    /**
     * Ruft alle Attribute des jeweiligen Raumschiffs ab und schreibt sie ins Terminal.
     */
    public void zustandRaumschiff(){
        System.out.println("\n\nRaumschiff " + schiffsname + "\n\nPhotonentorpedos: " + photonentorpedoAnzahl +
                            "\nEnergieversogung: " + energieversorgungInProzent + " %" +
                            "\nSchilde: " + schildeInProzent + " %" +
                            "\nHuelle: " + huelleInProzent + " %" +
                            "\nLebenserhaltungssysteme: " + lebenserhaltungssystemeInProzent + " %" +
                            "\nAndroiden: " + androidenAnzahl);
    }

    public void ladungsverzeichnisAusgeben() {
        System.out.println(ladungsverzeichnis.toString());
    }

    /**
     * Gibt das (static) Logbuch zurück.
     * @return String-ArrayList broadcastKommunikator
     */
    public static ArrayList<String> eintraegeLogbuchZurueckgeben(){
        return broadcastKommunikator;
    }

    /**
     * Lädt Torpedos. Wenn gewünschte Anzahl größer als verfügbare Ladung ist, lädt die Methode die gesamten geladenen
     * Torpedos.
     * @param anzahlTorpedos Anzahl der zu ladenden Torpedos
     */
    public void photonentorpedosLaden(int anzahlTorpedos){
        int ladungsindex = -1;
        //finde Index von Torpedos in Ladung
        for (int i=0; i<ladungsverzeichnis.size(); i++) {
            if (ladungsverzeichnis.get(i).getBezeichnung() == "Photonentorpedo"){
                ladungsindex = i;
                break;
            }
        }
        //wenn nicht als Ladung vorhanden oder Menge 0
        if (ladungsindex == -1 || ladungsverzeichnis.get(ladungsindex).getMenge() == 0) {
            nachrichtAnAlle("-=*Click*=-");
        }
        else if (ladungsverzeichnis.get(ladungsindex).getMenge() < anzahlTorpedos){
            setPhotonentorpedoAnzahl(getPhotonentorpedoAnzahl() + ladungsverzeichnis.get(ladungsindex).getMenge());
            System.out.println(ladungsverzeichnis.get(ladungsindex).getMenge() + " Photonentorpedo(s) eingesetzt");
            ladungsverzeichnis.get(ladungsindex).setMenge(0);
        }
        else {
            setPhotonentorpedoAnzahl(getPhotonentorpedoAnzahl() + anzahlTorpedos);
            System.out.println(anzahlTorpedos + " Photonentorpedo(s) eingesetzt");
            ladungsverzeichnis.get(ladungsindex).setMenge(anzahlTorpedos);
        }
    }

    /**
     * Leert die komplette Ladung aus dem Raumschiff.
     */
    public void ladungsverzeichnisAufraeumen(){
        for (Ladung x:ladungsverzeichnis) {
            if (x.getMenge() <= 0){
                ladungsverzeichnis.remove(x);
            }
        }
    }

    /**
     * Repariert bestimmte Schiffsteile je nach Wunsch. Höhe ist unter anderem abhängig von der übergebenen Anzahl der
     * Droiden.
     * @param schutzschilde Boolean, ob die Schutzschilde repariert werden sollen.
     * @param energieversorgung Boolean, ob die Energieversorgung repariert werden soll.
     * @param schiffshuelle Boolean, ob die Schiffshülle repariert werden sollen.
     * @param anzahlDroiden Anzahl, der für die Reparatur zu benutzenden Droiden.
     */
    public void reparaturDurchfuehren(boolean schutzschilde,
                                      boolean energieversorgung,
                                      boolean schiffshuelle,
                                      int anzahlDroiden){
        boolean[] module = new boolean[3];
        module[0] = schutzschilde;
        module[1] = energieversorgung;
        module[2] = schiffshuelle;

        int anzahlModule = 0;
        for (Boolean x:module) {
            if (x){
                anzahlModule = anzahlModule + 1;
            }
        }

        int random = ThreadLocalRandom.current().nextInt(0, 101);

        int berechnungDroiden = anzahlDroiden;
        if (anzahlDroiden > androidenAnzahl){
            berechnungDroiden = androidenAnzahl;
        }

        int reparaturwert = random * berechnungDroiden / anzahlModule;

        if (schutzschilde){
            setSchildeInProzent(getSchildeInProzent() + reparaturwert);
        }
        if (energieversorgung){
            setEnergieversorgungInProzent(getEnergieversorgungInProzent() + reparaturwert);
        }
        if (schiffshuelle){
            setHuelleInProzent(getHuelleInProzent() + reparaturwert);
        }
    }
}
